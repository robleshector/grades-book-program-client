import React, {useState, useEffect} from 'react';
import Navbar from './components/layouts/Navbar';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import CreateRecords from './components/CreateRecords';
import ShowRecords from './components/ShowRecords';

function App() {

  const [years, setYears] = useState([]);

  useEffect(() => {
    fetch('https://hjtr-gradebook-server.herokuapp.com/years',
      {
        method: "GET",
        headers: {
            "Content-Type" : "application/json"
        }
      }
    )
    .then(data => data.json())
    .then(list => {
      setYears([...list])
    })
  })

  return (
    <Router>
      <Navbar />
      <div className="container">
        <Switch>
          <Route exact path="/">
              <CreateRecords />
          </Route>
          
          <Route exact path="/index">
              <ShowRecords years={years} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
