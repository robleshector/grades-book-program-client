import React, {Fragment, useState} from 'react';

const ShowRecords = ({years}) => {

    const [records, setRecords] = useState([]);

    const handleYearChange = e => {

        fetch('https://hjtr-gradebook-server.herokuapp.com/records/' + e.target.value,
            {
                method: "GET",
                headers: {
                    "Content-Type" : "application/json"
                }
            }
        )
        .then(data => data.json())
        .then(list => {
            setRecords([...list])
        })


    }

    function strCapitalize(str) {
        return str.replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }


    return (
        <Fragment>
            <div className="container">
                <div className="row mt-3">
                    <div className="col-lg-4">
                        <div className="card mb-3">
                            <div className="card-header">
                                Show Records
                            </div>
                            <div className="card-body">
                                <div className="input-group mb-3">
                                    <div className="input-group-prepend">
                                        <label htmlFor="year" className="input-group-text">School Year:</label>
                                    </div>
                                    <select className="form-control" name="year" id="year" defaultValue="Select a Year" onChange={handleYearChange}>
                                        <option disabled>Select a Year</option>
                                        {years.map(year => {
                                            return (
                                                <option value={year._id} key={year._id}>{year.name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    {records[0] ? 
                    <div className="col-lg-8">
                        <div className="card">
                            <div className="card-header">
                                Results
                            </div>
                            <div className="card-body">
                                <table className="table table-hover table-bordered">
                                    <thead className="bg-light">
                                        <tr>
                                            <th>Name</th>
                                            <th className="text-center">Final Grade</th>
                                            <th className="d-none d-sm-table-cell text-center">Q1</th>
                                            <th className="d-none d-sm-table-cell text-center">Q2</th>
                                            <th className="d-none d-sm-table-cell text-center">Q3</th>
                                            <th className="d-none d-sm-table-cell text-center">Q4</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {records.map(record => {
                                            return (
                                                <tr key={record._id}>
                                                    <td>{strCapitalize(record.name)}</td>
                                                    <td className="text-center">{record.finalGrade}</td>
                                                    <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[0] ? Math.round(record.quarterGrades[0] * 10) / 10 : ""}</td>
                                                    <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[1] ? Math.round(record.quarterGrades[1] * 10) / 10 : ""}</td>
                                                    <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[2] ? Math.round(record.quarterGrades[2] * 10) / 10 : ""}</td>
                                                    <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[3] ? Math.round(record.quarterGrades[3] * 10) / 10 : ""}</td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    : ""}
                </div>
            </div>
        </Fragment>
        
    )
}

export default ShowRecords;