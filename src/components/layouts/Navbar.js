import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';

const Navbar = () => {
    return (
        <Fragment>
            <nav className="navbar navbar-dark navbar-expand-sm">
                <div className="container">
                    <Link to="/" className="navbar-brand">GradeBook</Link>
        				<button
        					className="navbar-toggler"
        					type="button"
        					data-toggle="collapse"
        					data-target="#navbarNav"
        				>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link to="/" className="nav-link">Add Records</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/index" className="nav-link">View Records</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </Fragment>
    )
}

export default Navbar;