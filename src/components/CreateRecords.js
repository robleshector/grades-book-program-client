import React, {Fragment, useState} from 'react';

const CreateRecords = () => {

    const [formData, setFormData] = useState({
        input : null
    });

    const [processedRecords, setProcessedRecords] = useState(null);

    const [result, setResult] = useState(null);

    const handleInputChange = e => {
        setFormData({
            input : e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault();

        setProcessedRecords(null);
        
        fetch('https://hjtr-gradebook-server.herokuapp.com/records/',
            {
                method : "POST",
                body : JSON.stringify(formData),
                headers: {
                    "Content-Type" : "application/json"
                }
            }
        )
        .then( data => data.json())
        .then( records => {
            if(!records[0]) {
                setProcessedRecords(records);
                setResult(<div className="alert alert-success text-center mb-0">Input processed successfully!</div>)
            } else {
                setResult(<div className="alert alert-danger text-center mb-0">Failed to process input. Please check format.</div>)
            }
        })
    }

    const handleFile = e => {
        const file = e.target.files[0];
        if(file) {
            const reader = new FileReader();
            reader.onload = e => {
                document.querySelector('#input').innerHTML = e.target.result;
                document.querySelector('#input').value = e.target.result;
                setFormData({
                    input : e.target.result
                })
            }
            reader.readAsText(file);
        }
    }

    function strCapitalize(str) {
        return str.replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    return (

        <Fragment>
            <div className="container">
                <div className="row mt-3">
                    <div className="col-lg-6">
                        <div className="card mb-3">
                            <div className="card-header">
                                Add Records
                            </div>
                            <div className="card-body">
                                <form onSubmit={handleSubmit}>
                                    <input 
                                        type="file" 
                                        className="mb-3"
                                        accept=".txt"
                                        onChange={handleFile}
                                    />
                                    <div className="form-group">
                                        <label htmlFor="input">Input:</label>
                                        <textarea name="input" id="input" rows="10" className="form-control" placeholder="Enter Data here" onChange={handleInputChange} required></textarea>
                                    </div>
                                    <div className="text-right mb-3">
                                        <button className="btn btn-primary px-5" type="submit" >Submit</button>
                                    </div>
                                    {result ? result : ""}
                                </form>
                            </div>
                        </div>
                        <div className="col-12">
                            <p className="small mb-0"><strong>Sample Format: Quarter 1, 2019 FirstName LastName H 99 95 98 T 98 85 96 ...</strong></p>
                            <p className="small">Please upload a valid .txt file or enter a valid input. Submitting grades for an existing student will replace that quarter grades.</p>
                            <p className="text-danger small">Warning: Submitting names with no scores will erase the student's grade for that quarter.</p>
                        </div>
                    </div>
                    
                    <div className="col-lg-6">
                    {processedRecords !== null ? 
                        <div className="card">
                            <div className="card-header">
                                Result
                            </div>
                            <div className="card-body">
                                {processedRecords.created && processedRecords.created[0] !== undefined ? 
                                    <Fragment>
                                        <div className="col-12">
                                            <h5>Created Records</h5>
                                        </div>
                                        <table className="table table-hover table-bordered">
                                            <thead className="bg-light">
                                                <tr>
                                                    <th>Name</th>
                                                    <th className="text-center">Final Grade</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q1</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q2</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q3</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q4</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {processedRecords.created.map(record => {
                                                    return (
                                                        <tr key={record._id}>
                                                            <td>{strCapitalize(record.name)}</td>
                                                            <td className="text-center">{record.finalGrade}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[0] ? Math.round(record.quarterGrades[0] * 10) / 10 : ""}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[1] ? Math.round(record.quarterGrades[1] * 10) / 10 : ""}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[2] ? Math.round(record.quarterGrades[2] * 10) / 10 : ""}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[3] ? Math.round(record.quarterGrades[3] * 10) / 10 : ""}</td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </table>
                                    </Fragment>
                                : ""}

                                {processedRecords.updated && processedRecords.updated[0] !== undefined ? 
                                    <Fragment>
                                        <div className="col-12">
                                            <h5>Updated Records</h5>
                                        </div>
                                        <table className="table table-hover table-bordered">
                                            <thead className="bg-light">
                                                <tr>
                                                    <th>Name</th>
                                                    <th className="text-center">Final Grade</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q1</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q2</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q3</th>
                                                    <th className="d-none d-sm-table-cell text-center">Q4</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {processedRecords.updated.map(record => {
                                                    return (
                                                        <tr key={record._id}>
                                                            <td>{strCapitalize(record.name)}</td>
                                                            <td className="text-center">{record.finalGrade}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[0] ? Math.round(record.quarterGrades[0] * 10) / 10 : ""}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[1] ? Math.round(record.quarterGrades[1] * 10) / 10 : ""}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[2] ? Math.round(record.quarterGrades[2] * 10) / 10 : ""}</td>
                                                            <td className="d-none d-sm-table-cell text-center">{record.quarterGrades[3] ? Math.round(record.quarterGrades[3] * 10) / 10 : ""}</td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </table>
                                    </Fragment>
                                : ""}
                            </div>
                        </div>
                    : ""}
                    </div>
                </div>
            </div>


        </Fragment>

    )
}

export default CreateRecords;